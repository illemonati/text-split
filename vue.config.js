module.exports = {
    transpileDependencies: ["vuetify"],
    publicPath: process.env.BASE_URL,
    pwa: {
        name: 'Text Split',
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: 'translucent',
        workboxPluginMode: 'GenerateSW',
    },

};
